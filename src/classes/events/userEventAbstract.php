<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\user\events;

use stdClass;
use Symfony\Contracts\EventDispatcher\Event;

abstract class userEventAbstract extends Event
 {
  // @var stdClass $user object to output a user
  protected $user;

  public function __construct(stdClass $user)
  {
      $this->user = $user;
  }

  public function getUser(): stdClass
  {
    return $this->user;
  }

  public function setUser(stdClass $user): void
  {
    $this->user = $user;
  }
 }