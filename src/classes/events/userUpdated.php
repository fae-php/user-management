<?php

/**
 * FAE 
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2020 Callum Smith
 */

namespace FAE\user\events;

class userUpdated extends userEventAbstract
{
  public const NAME = 'user.set.update';
}
