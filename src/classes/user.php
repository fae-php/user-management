<?php

namespace FAE\user;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Query\Expression\CompositeExpression;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use FAE\auth\auth;
use FAE\event_handler\eventHandler;
use FAE\permissions\perm;
use FAE\permissions\permAbstract;
use FAE\user\events\userCreated;
use FAE\user\events\userUpdated;

class user extends permAbstract
{

  var $_model     = 'user';
  var $_modelFile = __DIR__ . '/../models/user.json';
  var $_permRef   = 'user';

  var $_rest      = false;

  function getQueryHook($qb)
  {
    $qb->addSelect("CONCAT_WS(' ', t1.`first_name`, t1.`last_name`) AS `cn`");
    return $qb;
  }

  function setCallback(array $data, array $filter, string $type): array
  {
    $dispatcher = eventHandler::getHandler()->getDispatcher();
    $user = $this->get(array_key_exists('id', $data) ? ['id' => $data['id']] : $filter)->fetch();
    if ($type === 'insert') {
      $userCreatedEvent = new userCreated((object) $user);
      $dispatcher->dispatch($userCreatedEvent, userCreated::NAME);
    } else {
      $userUpdatedEvent = new userUpdated((object) $user);
      $dispatcher->dispatch($userUpdatedEvent, userUpdated::NAME);
    }
    return $data;
  }

  function permissionQBInstance(QueryBuilder $qb, int $level, $userId = null): QueryBuilder
  {
    if ($this->isDirect()) {
      return $qb;
    }
    $ah = new auth();
    $auth = $ah->get();
    $currentUser = null;
    if ($auth->isLoggedIn()) {
      $currentUser = $qb->expr()->eq("t1.`id`", $qb->createNamedParameter($auth->getUserID()));
    }
    $ph = new perm();
    $permissionTest = $ph->buildStandardPermissions($qb, $level, $this, $userId);
    $test = $qb->expr()->orX($currentUser, $permissionTest);
    $qb->andWhere($test);
    return $qb;
  }
}
